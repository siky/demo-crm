<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\EmployeeController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/dashboard', function () {
    return redirect()->route('company.index');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

Route::resource('company', CompanyController::class)->except(['show']);
Route::get('company/{company}/employees', 'App\Http\Controllers\CompanyController@employees')->name('company.employees');
Route::get('company/{company}/employee/create', 'App\Http\Controllers\EmployeeController@create')->name('company.employee.create');
Route::resource('employee', EmployeeController::class)->except(['show']);
