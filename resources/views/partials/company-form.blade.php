<label class="block">
    <span class="text-gray-700">Name</span>
    <input name="name" type="text" class="mt-1 block w-full {{ $errors->has('name') ? ' border-red-600' : '' }}" value="{{ old('name', $company?->name) }}">
</label>
@if ($errors->has('name'))
    <span class="text-red-600"><strong>{{ $errors->first('name') }}</strong></span>
@endif
<label class="block">
    <span class="text-gray-700">Email</span>
    <input name="email" type="text" class="mt-1 block w-full {{ $errors->has('email') ? ' border-red-600' : '' }}" placeholder="john@example.com" value="{{ old('email', $company?->email) }}">
    @if ($errors->has('email'))
        <span class="text-red-600"><strong>{{ $errors->first('email') }}</strong></span>
    @endif
</label>
<label class="block">
    <span class="text-gray-700">Logo</span>
    <input name="logo" type="file" class="mt-1 block w-full">
    @if($company?->logo) <img src="{{ '/logos/'.$company?->logo }}" alt="{{ $company->name }} logo" style="max-width: 100px" class="mt-2"> @endif
</label>
<label class="block">
    <span class="text-gray-700">Website</span>
    <input name="website" type="text" class="mt-1 block w-full" value="{{ old('website', $company?->website) }}">
</label>

<button class="px-6 py-2 rounded bg-slate-400 hover:bg-slate-500 text-slate-100 mt-6" type="submit">Save</button>
