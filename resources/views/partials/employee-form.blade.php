<label class="block">
    <span class="text-gray-700">Firstname</span>
    <input name="firstname" type="text" class="mt-1 block w-full {{ $errors->has('firstname') ? ' border-red-600' : '' }}" value="{{ old('firstname', $employee?->firstname) }}">
</label>
@if ($errors->has('firstname'))
    <span class="text-red-600"><strong>{{ $errors->first('firstname') }}</strong></span>
@endif
<label class="block">
    <span class="text-gray-700">Lastname</span>
    <input name="lastname" type="text" class="mt-1 block w-full {{ $errors->has('lastname') ? ' border-red-600' : '' }}" value="{{ old('lastname', $employee?->lastname) }}">
</label>
@if ($errors->has('lastname'))
    <span class="text-red-600"><strong>{{ $errors->first('lastname') }}</strong></span>
@endif
<label class="block">
    <span class="text-gray-700">Email</span>
    <input name="email" type="text" class="mt-1 block w-full {{ $errors->has('email') ? ' border-red-600' : '' }}" placeholder="john@example.com" value="{{ old('email', $employee?->email) }}">
    @if ($errors->has('email'))
        <span class="text-red-600"><strong>{{ $errors->first('email') }}</strong></span>
    @endif
</label>
<label class="block">
    <span class="text-gray-700">Phone</span>
    <input name="phone" type="text" class="mt-1 block w-full" value="{{ old('phone', $employee?->phone) }}">
</label>
<label class="block">
    <span class="text-gray-700">Company</span>
    <select name="company" class="mt-1 block w-full">
        <option value=""> -- no company selected --</option>
        @foreach($companies as $company)
            <option value="{{ $company->id }}" @if(in_array($company->id, [old('company', $employee?->company), $company_id])) selected @endif>{{ $company->name }}</option>
        @endforeach
    </select>
</label>

<button class="px-6 py-2 rounded bg-slate-400 hover:bg-slate-500 text-slate-100 mt-6" type="submit">Save</button>
