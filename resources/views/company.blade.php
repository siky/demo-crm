<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Companies') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <a href="{{ @route('company.create') }}" class="float-right">
                        <i class="fa fa-plus-circle"></i>
                    </a>
                    <table class="border-collapse table-auto w-full text-sm">
                        <thead>
                        <tr>
                            <th class="border-b font-medium p-4 pl-8 pt-0 pb-3 text-gray-400 text-left">Company</th>
                            <th class="border-b font-medium p-4 pt-0 pb-3 text-gray-400 text-left">Logo</th>
                            <th class="border-b font-medium p-4 pr-8 pt-0 pb-3 text-gray-400 text-left">Email</th>
                            <th class="border-b font-medium p-4 pr-8 pt-0 pb-3 text-gray-400 text-left">Total employees</th>
                            <th class="border-b font-medium p-4 pr-8 pt-0 pb-3 text-gray-400 text-left">Edit</th>
                        </tr>
                        </thead>
                        <tbody class="bg-white">
                        @foreach($companies as $company)
                            <tr>
                                <td class="border-b border-gray-100 p-4 pl-8 text-gray-500">{{ $company->name }}</td>
                                <td class="border-b border-gray-100 p-4 text-gray-500">
                                    @if($company->logo) <img src="{{ '/logos/'.$company->logo }}" alt="{{ $company->name }} logo" style="max-width: 100px"> @endif
                                </td>
                                <td class="border-b border-gray-100 p-4 pr-8 text-gray-500">{{ $company->email }}</td>
                                <td class="border-b border-gray-100 p-4 pr-8 text-gray-500">
                                    <a href="{{ @route('company.employees', $company->id) }}">{{ $company->employees()?->count() }}</a>
                                </td>
                                <td class="border-b border-gray-100 p-4 pr-8 text-gray-500">
                                    <a href="{{ @route('company.edit', $company->id) }}"><i class="fa fa-edit"></i></a>
                                    <a href="#" class="delete" data-name="{{ $company->name }} ({{ $company->id }})" data-id="{{ $company->id }}"><i class="fa fa-trash"></i></a>
                                    <form action="{{ @route('company.destroy', $company->id) }}" method="post" id="{{ $company->id }}">
                                        @csrf
                                        @method('DELETE')
                                        <input type="submit" style="display: none">
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</x-app-layout>
