<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            @if(!empty($company)) {{ $company->name }} - @endif {{ __('Employees') }}
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <a href="@if(!empty($company)){{ @route('company.employee.create', $company->id) }} @else {{ @route('employee.create') }} @endif" class="float-right">
                        <i class="fa fa-plus-circle"></i>
                    </a>

                    <table class="border-collapse table-auto w-full text-sm">
                        <thead>
                        <tr>
                            <th class="border-b font-medium p-4 pl-8 pt-0 pb-3 text-gray-400 text-left">Name</th>
                            <th class="border-b font-medium p-4 pt-0 pb-3 text-gray-400 text-left">Company</th>
                            <th class="border-b font-medium p-4 pr-8 pt-0 pb-3 text-gray-400 text-left">Email</th>
                            <th class="border-b font-medium p-4 pr-8 pt-0 pb-3 text-gray-400 text-left">Phone</th>
                            <th class="border-b font-medium p-4 pr-8 pt-0 pb-3 text-gray-400 text-left">Edit</th>
                        </tr>
                        </thead>
                        <tbody class="bg-white">
                        @foreach($employees as $employee)
                            <tr>
                                <td class="border-b border-gray-100 p-4 pl-8 text-gray-500">{{ $employee->firstname }} {{ $employee->lastname }}</td>
                                <td class="border-b border-gray-100 p-4 text-gray-500">{{ $employee->company()?->name }}</td>
                                <td class="border-b border-gray-100 p-4 text-gray-500">{{ $employee->email }}</td>
                                <td class="border-b border-gray-100 p-4 pr-8 text-gray-500">{{ $employee->phone }}</td>
                                <td class="border-b border-gray-100 p-4 pr-8 text-gray-500">
                                    <a href="{{ @route('employee.edit', $employee->id) }}"><i class="fa fa-edit"></i></a>
                                    <a href="#"
                                       class="delete"
                                       data-name="{{ $employee->firstname }} {{ $employee->lastname }} ({{ $employee->id }})"
                                       data-id="{{ $employee->id }}"
                                    >
                                        <i class="fa fa-trash"></i>
                                    </a>
                                    <form action="{{ @route('employee.destroy', $employee->id) }}" method="post" id="{{ $employee->id }}">
                                        @csrf
                                        @method('DELETE')
                                        <input type="submit" style="display: none">
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</x-app-layout>
