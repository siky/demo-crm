<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl leading-tight">
            <span class="text-gray-800">{{ __('Create employee') }}</span>
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    <div class="grid grid-cols-1 gap-6">
                        <form method="post" action="{{ @route('employee.store') }}" enctype="multipart/form-data">
                            @csrf
                            @include('partials.employee-form')
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

</x-app-layout>
