<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Http\Request;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::all();
        return view('company')->with(compact('companies'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $company = null;
        return view('company-create')->with(compact('company'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'  => 'required',
            'email'  => 'email',
        ]);
        $data = $request->all();

        $company = Company::create($data);

        if(!empty($request->file('logo'))){
            $logoName = $company->id . '-' . $request->file('logo')->getClientOriginalName();
            $request->file('logo')->move(public_path('logos'), $logoName);
            $company->update(['logo' => $logoName]);
        }


        return redirect()->route('company.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::find($id);
        return view('company-edit')->with(compact('company'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'name'  => 'required',
            'email'  => 'email',
            'logo' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);
        $data = $request->all();
        $company = Company::find($id);

        if(!empty($request->file('logo'))){
            if(is_file(public_path('logos/'.$company->logo))){
                unlink(public_path('logos/'.$company->logo));
            }
            $logoName = $id . '-' . $request->file('logo')->getClientOriginalName();
            $request->file('logo')->move(public_path('logos'), $logoName);
            $data['logo'] = $logoName;
        }

        $company->update($data);

        return redirect()->route('company.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = Company::find($id);
        if(is_file(public_path('logos/'.$company->logo))){
            unlink(public_path('logos/'.$company->logo));
        }
        $company->destroy([$id]);

        return redirect()->back();
    }

    /**
     * @param $company_id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public function employees($company_id){
        $company = Company::find($company_id);
        $employees = $company->employees;
        return view('employee')->with(compact('company', 'employees'));
    }
}
