<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'firstname',
        'lastname',
        'company',
        'email',
        'phone',
    ];

    public function company(){
        return $this->belongsTo(Company::class, 'company')->first();
    }

}
