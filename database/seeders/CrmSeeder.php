<?php

namespace Database\Seeders;

use App\Models\Company;
use App\Models\Employee;
use Illuminate\Database\Seeder;

class CrmSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $company = Company::create([
            'name' => 'Tesztelő Kft',
            'email' => 'teszt@tesztelokft.hu',
            'logo' => 'teszt-logo.jpg',
            'website' => 'tesztelokft.hu',
        ]);

        Employee::query()->insert([
            'firstname' => 'Ernő',
            'lastname' => 'Tesztelő',
            'company' => $company->id,
            'email' => 'teszt@tesztelokft.hu',
            'phone' => '+36301234567',
        ]);
    }
}
